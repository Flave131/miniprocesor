#pragma once
#include "Memory.h"
#include <iostream>
#include <thread> 
#include <vector>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string>
#include <queue>
#include <mutex>
#include <stack>

using namespace std;


class logger {

public:

	logger() {};

	void PrintIP(string caller, uint16_t message);

	void PrintInfo(string caller, uint16_t ip, uint64_t data);

	void PrintData(string caller, uint64_t message);

	//void PrintFile(string caller, uint64_t message);

	void PrintSrc1(string code);
	void PrintSrc2(string code);
	void PrintOpcode(string code);

};

 class CPU {
	 
	 Memory *memory;
	 logger *ptrLog;

	 uint16_t flag[3];
	 uint16_t R[8];

	 stack <uint16_t> stack_base; //address for Stack Base
	 uint16_t stack_size; // size of Stack
	 uint16_t mem_base; // size of address for Memory Base
	 uint16_t mem_size; // size of memory
	 int stack_pointer; // stack pointer

	 

public:

	struct Instruct {

		uint16_t header;
		uint16_t optionalParameter1 = NULL;
		uint16_t optionalParameter2 = NULL;
	};

	struct CB {
		uint16_t ip;
		uint16_t dest;
		uint16_t value;
		bool address1;
		bool address2;
	};

	struct Date {
		uint16_t ip;
		int id;
	};

	CPU(logger* x, Memory *memory) {

		ptrLog = x;
		this->memory = memory;
	};

	uint32_t load(uint16_t addr);

	void store(uint16_t addr, uint16_t data);

	void Clock();

	void start_sim();
		
	void stop_sim();

	void IC();

	virtual void DE();

	void EX();

	void LS();

	void cBuffer();

	//uint64_t fetch(uint16_t ip) {};

};
