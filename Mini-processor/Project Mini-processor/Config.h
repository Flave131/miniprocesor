#pragma once

#define max_ex 4

#define delay_cBuff 1
#define delay_IC 3
#define delay_DE 2
#define delay_EX 5
#define delay_LS 6

#define mask_op 0xA & 0x3F
#define mask_src1 0x5 & 0x1F
#define mask_src2 0x1F

#define add 0x1
#define sub 0x2
#define mov 0x3
#define mul 0x4
#define div 0x5
#define cmp 0x6
#define jmp 0x7
#define je 0x9
#define jl 0xa
#define jg 0xb
#define jz 0xc
#define call 0xd
#define ret 0xe
#define end_sim 0xf
#define pushh 0x10
#define popp 0x11

#define R0 0x1
#define R1 0x2
#define R2 0x3
#define R3 0x4
#define R4 0x5
#define R5 0x6
#define R6 0x7
#define R7 0x8
#define IMM 0x10
#define ADDR 0x11
#define ADDR_R 0x12

#define Z 0
#define E 1	
#define G 2

#define thread_sleep chrono::milliseconds(10)
#define requested_sleep chrono::milliseconds(70)
