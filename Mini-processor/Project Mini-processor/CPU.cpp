#include "CPU.h"
#include "Memory.h"
#include "Config.h"
#include <fstream>
#include <string>
#include <stdio.h>
#include <inttypes.h>
#include <thread>
#include <set>
#include <time.h>
#include <chrono>
#include <mutex>
#include <unordered_map>

using namespace std;

int globalcycle;
bool run = true;
queue <uint16_t> exs;
queue<CPU::Instruct> for_update;

queue<uint64_t> DE_IC;
queue<uint64_t> DE_LS;
queue<uint64_t> LS_DE;
queue<uint64_t> EX_cBuff;
queue<uint16_t> window_ip;
queue<uint64_t> window_data;
queue<CPU::Instruct> hpp_DE_EX;
queue<CPU::Instruct> hpp_cBuff_EX;
queue<CPU::Instruct> vector_date;
std::vector<std::thread> threads;
mutex mtx;
condition_variable cv;
uint16_t header;
uint16_t saver = 0;
bool newIP = true;
bool necesar = false;
bool window = false;
ofstream file;
uint16_t firstWindow;
uint16_t secondWindow;
map <uint16_t, CPU::CB> row;

uint16_t IP;
map<uint16_t, uint64_t> instr;
queue<uint16_t> IPqueue;
queue<uint16_t> IC_LS; // for IP
queue<uint64_t> LS_IC; // for data
queue<uint64_t> IC_DE; // for data

std::mutex cout_mutex;
std::size_t counter = 0;
static std::unordered_map<std::thread::id, std::size_t> thread_ids;

uint64_t fetch(uint16_t ip) {
	ip = ip & ~7;
	IC_LS.push(ip);
	while (LS_IC.empty()) {
		this_thread::sleep_for(thread_sleep);
	}
	uint64_t data = LS_IC.front();
	LS_IC.pop();
	return data;
}

std::size_t get_thread_id() {
	static size_t thread_idx = 0;
	static mutex thread_mutex;
	
	lock_guard<mutex> lock(thread_mutex);
	thread::id id = this_thread::get_id();
	auto iter = thread_ids.find(id);
	if (iter == thread_ids.end()) {
		iter = thread_ids.insert(pair<thread::id, size_t>(id, thread_idx++)).first;
	}
	return iter->second;
	thread_mutex.unlock();
};

void CPU::IC() {

	while (run) {

		if (globalcycle % delay_IC == 0) {

			int target_cycle = globalcycle + delay_IC;

			while (target_cycle > globalcycle) {
				std::this_thread::sleep_for(requested_sleep);
			}

			string who = "IC";

			while (IPqueue.empty()) {
				this_thread::sleep_for(thread_sleep);
			}

			uint16_t ip = IPqueue.front();
			IPqueue.pop();

			if (instr.find(ip) == instr.end()) { // IP not found in map
				uint64_t data = fetch(ip);
				IC_DE.push(data);
				instr.insert(make_pair(ip, data)); // adding instruction in map
				ptrLog->PrintInfo(who, ip, data);
				IP = ip;
			}
			else { //IP & data already in map
				uint64_t data = instr[ip];
				IC_DE.push(data);
				ptrLog->PrintInfo(who, ip, data);
				IP = ip;
			}
			
		}
		else {
			std::this_thread::sleep_for(requested_sleep);
		}
	}
}

void CPU::LS() {

	while (run) {

		if (globalcycle % delay_LS == 0) {
			int target_cycle = globalcycle;
			while (target_cycle > globalcycle) {

			}
			string who = "LS";
			string whoLoad = "LS (load)";

			if (IC_LS.size() != 0) {
				uint16_t ip = IC_LS.front();
				IC_LS.pop();
				uint64_t data = memory->readM<uint64_t>(ip);
				LS_IC.push(data);
				ptrLog->PrintInfo(who, ip, data);
			}


			if (DE_LS.size() != 0) { //requested more DATA by DE
				uint16_t loadIp = DE_LS.front();
				uint64_t data = memory->readM<uint64_t>(loadIp);
				DE_LS.pop();
				LS_DE.push(data);
				ptrLog->PrintInfo(whoLoad, loadIp, data);
			}
		}
		else {
			std::this_thread::sleep_for(requested_sleep);
		}
	}
}


void CPU::DE() {

	uint16_t opcode;
	uint16_t src1;
	uint16_t src2;
	Instruct Instr;
	uint16_t srcSaver;
	uint64_t data = 0;
	bool done = true;
	int currentPosition;
	uint16_t vector[4];
	uint16_t newVector[4];
	

	while (run) {

		if (globalcycle % delay_DE == 0) {
			
			uint16_t ip1 = IP;

			int target_cycle = globalcycle;
			while (target_cycle > globalcycle) {
				std::this_thread::sleep_for(thread_sleep);
			}

			string send = "-- DE sends data to EX -- ";
			string get = "-- DE received data from IC -- ";

			
			if (IC_DE.size() != 0) { //daca am data in IC_DE
				data = IC_DE.front();
				IC_DE.pop();
				ptrLog->PrintData(get, data);
				memcpy(vector, &data, sizeof(uint64_t));
				currentPosition = ((ip1 >> 1) % 4);
				done = false; // am luat data si urmeaza sa o parcurg

			LOOP: while (done == false && currentPosition < 4) {
				//window = true;
				Instr.optionalParameter1 = 0;
				Instr.optionalParameter2 = 0;
				bool parameter1 = false;
				bool parameter2 = false;
				int size = 0;
				

				header = vector[currentPosition];

				src2 = header & mask_src2;
				src1 = header >> mask_src1;
				opcode = header >> mask_op;

				switch (opcode) {
				case add:
					ptrLog->PrintOpcode("ADD");
					break;
				case sub:
					ptrLog->PrintOpcode("SUB");
					break;
				case mov:
					ptrLog->PrintOpcode("MOV");
					break;
				case mul:
					ptrLog->PrintOpcode("MUL");
					break;
				case div:
					ptrLog->PrintOpcode("DIV");
					break;
				case cmp:
					ptrLog->PrintOpcode("CMP");
					break;
				case jmp:
					ptrLog->PrintOpcode("JMP");
					break;
				case je:
					ptrLog->PrintOpcode("JE");
					break;
				case jl:
					ptrLog->PrintOpcode("JL");
					break;
				case jg:
					ptrLog->PrintOpcode("JG");
					break;
				case jz:
					ptrLog->PrintOpcode("JZ");
					break;
				case call:
					ptrLog->PrintOpcode("CALL");
					break;
				case ret:
					ptrLog->PrintOpcode("RET");
					break;
				case end_sim:
					ptrLog->PrintOpcode("END SIM");
					break;
				case pushh:
					ptrLog->PrintOpcode("PUSH");
					break;
				case popp:
					ptrLog->PrintOpcode("POP");
					break;
				default:
					ptrLog->PrintOpcode("Not valid");
					header = 0x0;
					break;
				}

				if (src1 == ADDR || src1 == ADDR_R || src1 == IMM) { parameter1 = true; size++; }
				if (src2 == ADDR || src2 == ADDR_R || src2 == IMM) { parameter2 = true; size++; }

				if (currentPosition == 0 || currentPosition == 1) {
					if (parameter1) {
						Instr.optionalParameter1 = load(vector[++currentPosition]);
						currentPosition++;
					}
					if (parameter2) { Instr.optionalParameter2 = load(vector[++currentPosition]); }

					if (header == 0x0) {
						cout << "Nope" << endl;
						currentPosition++;
						goto LOOP;
					}
					else {
						Instr.header = header;
						vector_date.push(Instr);
						//cout << "Complete instruction: " << endl << Instr.header << "  " << Instr.optionalParameter1 << "  " << Instr.optionalParameter2 << endl;
						currentPosition++;
						goto LOOP;
					}
				}

				if (currentPosition == 2) {
					if (size == 0) {
						if (header == 0x0) {
							cout << "Nope" << endl;
							currentPosition++;
							goto LOOP;
						}
						else {
							Instr.header = header;
							vector_date.push(Instr);
							currentPosition++;
							goto LOOP;
						}
					}
					if (size == 1 && parameter1 == true) {
						Instr.optionalParameter1 = vector[++currentPosition];
						Instr.header = header;
						//cout << "Complete instruction: " << endl << Instr.header << "  " << Instr.optionalParameter1 << "  " << Instr.optionalParameter2 << endl;
						vector_date.push(Instr);
						currentPosition++;
						done = true;
						goto LOOP;
					}
					if (size == 2) {
						//cout << "Incomplete instruction" << endl;
						Instr.header = header;
						Instr.optionalParameter1 = vector[++currentPosition];
						currentPosition++; //cP = 4 -> stop while

						ip1 = ip1 + 8;
						IPqueue.push(ip1); // send to IC

						while (IC_DE.empty()) { //recieve data from IC
							std::this_thread::sleep_for(thread_sleep);
						}

						if (IC_DE.size() != 0) {
							uint64_t newWindow = IC_DE.front();
							IC_DE.pop();
							memcpy(newVector, &newWindow, sizeof(uint64_t));
							int index = 0;
							src2 = newVector[index];
							Instr.optionalParameter2 = load(src2);
							cout << "Complete instruction: " << endl << Instr.header << "  " << Instr.optionalParameter1 << "  " << Instr.optionalParameter2 << endl;

						}

						vector_date.push(Instr);
						done = true; // stop while
						goto LOOP;

					}
				}

				if (currentPosition == 3) {
					if (size == 0) {
						if (header == 0x0) {
							cout << "Nope" << endl;
							currentPosition++;
							goto LOOP;
						}
						else {
							Instr.header = header;
							//cout << "Instruction: " << endl << Instr.header << "  " << Instr.optionalParameter1 << "  " << Instr.optionalParameter2 << endl;
							vector_date.push(Instr);
							currentPosition++;
							done = true;
							goto LOOP;
						}
					}

					if (size == 1) {

						ip1 = ip1 + 8;
						IPqueue.push(ip1); // send to IC

						while (IC_DE.empty()) { //recieve data from IC
							std::this_thread::sleep_for(thread_sleep);
						}

						if (IC_DE.size() != 0) {
							uint64_t newWindow = IC_DE.front();
							IC_DE.pop();
							memcpy(newVector, &newWindow, sizeof(uint64_t));
							int index = 0;
							src1 = newVector[index];
							uint16_t data = load(src1);
							src1 = data;

							Instr.header = header;
							Instr.optionalParameter1 = src1;
							Instr.optionalParameter2 = 0;
							cout << "Complete instruction: " << endl << Instr.header << "  " << Instr.optionalParameter1 << "  " << Instr.optionalParameter2 << endl;
						}

						vector_date.push(Instr);
						done = true; // stop while
						goto LOOP;
					}

					if (size == 2) {

						Instr.header = header;
						ip1 = ip1 + 8;
						IPqueue.push(ip1); // send to IC

						while (IC_DE.empty()) { //recieve data from IC
							std::this_thread::sleep_for(thread_sleep);
						}

						if (IC_DE.size() != 0) {
							uint64_t newWindow = IC_DE.front();
							IC_DE.pop();
							memcpy(newVector, &newWindow, sizeof(uint64_t));
							int index = 0;
							src1 = newVector[index];
							src2 = newVector[index + 1];

							Instr.optionalParameter1 = load(src1); //valoarea adresei (adresa 0004 -> val 0c24)

							Instr.optionalParameter2 = load(src2);
						
							cout << "Complete instruction: " << endl << Instr.header << "  " << Instr.optionalParameter1 << "  " << Instr.optionalParameter2 << endl;
						}
						vector_date.push(Instr);
						done = true; // stop while
						goto LOOP;
					}
				}
			} //while
				  if (currentPosition >= 3) {
					  //am parcurs data si trimit la EX
					  int size = vector_date.size();
					  cout << endl << "Number of instr.: " << size << endl << endl;

					  for (int i = 1; i < size + 1; i++) {
						  if (vector_date.size() != 0) {
							  Instr = vector_date.front();
							  vector_date.pop();
						  }
						  else {
							  cout << "Empty queue in DE" << endl;
						  }
						  //cout << "****** Instruction no. : " << i << endl << Instr.header << "  " << Instr.optionalParameter1 << "  " << Instr.optionalParameter2 << endl;
						  hpp_DE_EX.push(Instr);
					  }
					  window = false;
				  }
				  else goto LOOP;
			}
		}
		else {
			//cout << " CLK " << globalcycle << "  is not right time for DE" << endl;
			std::this_thread::sleep_for(requested_sleep);
		}
	}
}

void CPU::EX() {
	//cout << "Working with EX thread id : " << this_thread::get_id() << endl;
	while (run) {

		if (globalcycle % delay_EX == 0) {

			Instruct Instr;
			CB cbuffer;
			uint16_t src1 = 0;
			uint16_t src2 = 0;
			uint16_t ip = IP;
			uint16_t dest;
			//uint16_t ip_l = ip & ~7;
			//uint16_t ip_h = (ip + 8) & ~7;
			string get = "-- EX recieved data from DE -- ";

			int target_cycle = globalcycle;
			while (target_cycle > globalcycle) {
				std::this_thread::sleep_for(thread_sleep);
			}

			if (hpp_DE_EX.size() != 0) {

					Instr = hpp_DE_EX.front(); // scot o instrctiune
					hpp_DE_EX.pop();

					//for_update.push(Instr);

					uint16_t instr = Instr.header;
					ptrLog->PrintData(get, instr);

					src2 = instr & 0x1F;
					src1 = (instr >> 0x5) & 0x1F;
					uint16_t opcode = (instr >> 0xA) & 0x3F;

					ip = ip + 2;

					switch (src1) {
					case R0:
						cbuffer.address1 = false;
						ptrLog->PrintSrc1("R0");
						break;
					case R1:
						cbuffer.address1 = false;
						ptrLog->PrintSrc1("R1");
						break;
					case R2:
						cbuffer.address1 = false;
						ptrLog->PrintSrc1("R2");
						break;
					case R3:
						cbuffer.address1 = false;
						ptrLog->PrintSrc1("R3");
						break;
					case R4:
						cbuffer.address1 = false;
						ptrLog->PrintSrc1("R4");
						break;
					case R5:
						cbuffer.address1 = false;
						ptrLog->PrintSrc1("R5");
						break;
					case R6:
						cbuffer.address1 = false;
						ptrLog->PrintSrc1("R6");
						break;
					case R7:
						cbuffer.address1 = false;
						ptrLog->PrintSrc1("R7");
						break;
					case IMM:
						cbuffer.address1 = true;
						ip = ip + 2;
						ptrLog->PrintSrc1("IMM");
						break;
					case ADDR:
						cbuffer.address1 = true;
						ip = ip + 2;
						ptrLog->PrintSrc1("ADDR");
						break;
					case ADDR_R:
						cbuffer.address1 = true;
						ip = ip + 2;
						ptrLog->PrintSrc1("ADDR_R");
						break;
					default:
						cbuffer.address1 = false;
						ptrLog->PrintSrc1("Not valid");
						break;
					}


					switch (src2) {
					case R0:
						cbuffer.address2 = false;
						ptrLog->PrintSrc2("R0");
						break;
					case R1:
						cbuffer.address2 = false;
						ptrLog->PrintSrc2("R1");
						break;
					case R2:
						cbuffer.address2 = false;
						ptrLog->PrintSrc2("R2");
						break;
					case R3:
						cbuffer.address2 = false;
						ptrLog->PrintSrc2("R3");
						break;
					case R4:
						cbuffer.address2 = false;
						ptrLog->PrintSrc2("R4");
						break;
					case R5:
						cbuffer.address2 = false;
						ptrLog->PrintSrc2("R5");
						break;
					case R6:
						cbuffer.address2 = false;
						ptrLog->PrintSrc2("R6");
						break;
					case R7:
						cbuffer.address2 = false;
						ptrLog->PrintSrc2("R7");
						break;
					case IMM:
						ip = ip + 2;
						cbuffer.address2 = true;
						ptrLog->PrintSrc2("IMM");
						break;
					case ADDR:
						ip = ip + 2;
						cbuffer.address2 = true;
						ptrLog->PrintSrc2("ADDR");
						break;
					case ADDR_R:
						ip = ip + 2;
						cbuffer.address2 = true;
						ptrLog->PrintSrc2("ADDR_R");
						break;
					default:
						ptrLog->PrintSrc2("Not valid");
						cbuffer.address2 = false;
						break;
					}


					if (Instr.optionalParameter2 != NULL) {
						src2 = Instr.optionalParameter2;
					}
					if (Instr.optionalParameter1 != NULL) {
						src1 = Instr.optionalParameter1;
					}

					switch (opcode) {
					case add:
						if (src1 != IMM) {
							dest = src1;
							src1 = src1 + src2;

							if (src1 == 0xffff || src1 == 0x0) flag[Z] = 1;

							cbuffer.dest = dest;
							cbuffer.value = src1;
						}
						else {
							cout << endl << "           ADD: Illegal source1" << endl;
						}
						break;

					case sub:
						if (src1 != IMM) {
							dest = src1;
							src1 = src1 - src2;
							if (src1 == 0xffff || src1 == 0x0)  flag[Z] = 1;

							cbuffer.dest = dest;
							cbuffer.value = src1;
						}
						else {
							cout << "           SUB: Illegal source1" << endl;
						}
						break;

					case mov:
						if (src1 != IMM) {
							dest = src1;
							src1 = src2;
							cbuffer.dest = dest;
							cbuffer.value = src1;
						}
						else {
							cout << endl << "           MOV: Illegal source1" << endl;
						}
						break;

					case mul:
						dest = src1;
						src1 = src1 * src2;
						cbuffer.dest = dest;
						cbuffer.value = src1;
						if (src1 == 0xffff || src1 == 0x0) {
							flag[Z] = 1;
						}
						break;

					case div:
						if (src2 != 0) {
							dest = src1;
							src1 = src1 / src2;
							R[0] = src1;
							R[1] = src1 % src2;
							ptrLog->PrintSrc1(to_string(src1));
							cbuffer.dest = dest;
							cbuffer.value = src1;
							if (src1 == 0xffff || src1 == 0x0) {
								flag[Z] = 1;
							}
						}
						else {
							cout << endl << "         Illegal source2" << endl;
						}
						break;

					case cmp:
						if (src1 == src2) { flag[E] = 1; flag[Z] = 0; flag[G] = 0; }
						else if (src1 == 0 && src2 == 0) { flag[Z] = 1; flag[E] = 0; flag[G] = 0; }
						else if (src1 > src2) { flag[G] = 1;  flag[Z] = 0; flag[E] = 0; }
						else { flag[Z] = 0; flag[E] = 0; flag[G] = 0; } //flag is Lower
						break;

					case jmp:
						ip = src1;
						break;

					case je:
						if (flag[E]) ip = src1;
						break;

					case jl:
						if (!flag[E] && !flag[G]) ip = src1;
						break;

					case jg:
						if (flag[G]) ip = src1;
						break;

					case jz:
						if (flag[Z]) ip = src1;
						break;

					case call:
						ip = src1;
						stack_base.push(ip + 2);
						break;

					case ret:
						R[0] = stack_base.top();
						stack_base.pop();
						ip = R[0];
						break;

					case end_sim:
						stop_sim();
						break;

					case pushh:
						stack_base.push(src1);
						stack_pointer--;
						break;

					case popp:
						R[2] = stack_base.top();
						//stack_base.pop();
						stack_pointer++;
						break;

					default:
						ptrLog->PrintOpcode("Error in EX");
					}

					cout << " Working with EX[" << get_thread_id() << "]" << endl;
					cbuffer.ip = ip;
					IPqueue.push(ip);
					// row[ip] = Instr; // map de IP & Instr
					row.insert(std::make_pair(ip, cbuffer));
					exs.push(ip); 
					counter++;
					this_thread::sleep_for(requested_sleep);
			}
			
		}
	}
}

void update(uint16_t &dest, uint16_t src) {
	dest = src;
}

void CPU::cBuffer() {

	while (run) {	

		/*set<uint16_t> arr;
		uint16_t ip_l;
		uint16_t ip_h;
		vector <uint16_t> ipuri;
		uint16_t address;
		CB buffer;

		while (exs.size() != 0 ) {
			
			address = exs.front();
			exs.pop();

			ip_l = IP & ~7;
			ip_h = (IP + 8) &~7;

			if (ip_l <= address && ip_h > address) { // daca este in interval il sortam
				ipuri.push_back(address);
			}
			else if (ip_l < address && !(address < ip_h)) { // daca este mai mare decat intervalul updatam IP
				IP = address; //actualizez IP-ul global
				IPqueue.push(IP);
				newIP = true;
			}
			else if (!(ip_l < address) && address < ip_h) { // daca este mai mic decat intervalul nostru facem drop
				cout << "Dropped the address " << address << endl;
			}
		}
		if (!ipuri.empty()) {
			sort(ipuri.begin(), ipuri.end()); //sortam IP-urile
			cout << "SORTED" << endl;
		}
		// else cout << "Where are the IPs?" << endl;

		int size = ipuri.size();
		for (int i = 0; i < size; i++) {
			uint16_t data = ipuri.front();
			ipuri.erase(ipuri.begin()); //sterg primul ip
			buffer = row[data];

			cout << "DEST: " << buffer.dest << endl << "SRC: " << buffer.value << endl;

			if  ( (buffer.address1 == true && buffer.address2 == true) || (buffer.address1 == true && buffer.address2 == false) ) {
				store(buffer.dest, buffer.value);
				update(buffer.dest, buffer.value);
			}
			else {
				update(buffer.dest, buffer.value);
			}
		}*/

	}
}




uint32_t CPU::load(uint16_t addr) {
	uint32_t data = 0;
	//cout << endl << "          Requesting more data" << endl;
	DE_LS.push(addr);
	while (data == 0) {
		if (LS_DE.size() != 0) {
			data = LS_DE.front();
			LS_DE.pop();
			return data;
		}
		else this_thread::sleep_for(thread_sleep);
	}
}

void CPU::store(uint16_t addr, uint16_t data) {
	memory->write(addr, data);
}


void CPU::Clock() {
	globalcycle = 0;
	while (run) {
		globalcycle++;
		std::this_thread::sleep_for(requested_sleep);
	}
}

void CPU::start_sim() {

	uint16_t  IP = 0x000;
	IPqueue.push(IP);

	// creating threads
	auto icThread = std::thread(&CPU::IC, this);
	thread::id id_ic = icThread.get_id();
	cout << "Thread IC ->" << id_ic << endl;

	auto deThread = std::thread(&CPU::DE, this);
	thread::id id_de = deThread.get_id();
	cout << "Thread DE ->" << id_de << endl;

	/*auto exThread = std::thread(&CPU::EX, this);
	thread::id id_ex = exThread.get_id();
	cout << "Thread EX" << endl;
	*/

	for (int j = 0; j < max_ex; j++)
	{
		threads.push_back(thread(&CPU::EX, this));
		cout << "Thread EX[" << j << "] ->" << threads[j].get_id() << endl;
	}

	//cout << "Synchronizing all threads...\n";
	for (auto& th : threads) th.detach();


	auto lsThread = std::thread(&CPU::LS, this);
	thread::id id_ls = lsThread.get_id();
	cout << "Thread LS ->" << id_ls << endl;

	auto cBuffThread = std::thread(&CPU::cBuffer, this);
	thread::id id_cBuff = cBuffThread.get_id();
	cout << "Thread cBuff ->" << id_cBuff << endl;
	
	auto clkThread = std::thread(&CPU::Clock, this);
	thread::id id_clk = clkThread.get_id();
	cout << "Thread CLK ->" << id_clk << endl;

	clkThread.detach();
	icThread.detach();
	deThread.detach();
	lsThread.detach();
	//exThread.detach();
	cBuffThread.detach();
}

void CPU::stop_sim() {
	run = false;
	cout << "Stopped! " << endl;
	file.close();
}

void logger::PrintIP(string caller, uint16_t message) {
	mutex mtx;
	condition_variable cv;
	unique_lock<mutex>locker(mtx);
	cv.wait_for(locker, thread_sleep);
	cout << dec << "\n Clk: " << globalcycle;
	cout << hex << "   Message: " << caller << "   IP: " << message << endl;


	//file.open("outputFile.txt", std::ios_base::app);
	//file << "CLK: " + to_string(globalcycle) + " Message: " + caller + "  " + to_string(message) << endl;

	locker.unlock();
	cv.notify_one();


}

void logger::PrintInfo(string caller, uint16_t ip, uint64_t data) {
	mutex mtx;
	condition_variable cv;
	unique_lock<mutex>locker(mtx);
	cv.wait_for(locker, thread_sleep);
	cout << dec << "\n Clk: " << globalcycle;
	cout << hex << "  "  << caller << " sends  IP: " << ip <<  " & data: " << data << endl;
	locker.unlock();
	cv.notify_one();
}

void logger::PrintData(string caller, uint64_t message) {
	mutex mtx;
	condition_variable cv;
	unique_lock<mutex>locker(mtx);
	cv.wait_for(locker, thread_sleep);
	cout << dec << "\n Clk: " << globalcycle;
	cout << hex << "   Message: " << caller << "   Data: " << message << endl;

	//file.open("outputFile.txt", std::ios_base::app);
	//file << "CLK: " + to_string(globalcycle) + " Message: " + caller + "  " + to_string(message) << endl;

	locker.unlock();
	cv.notify_one();
}



void logger::PrintSrc1(string code) {
	mutex mtx;
	condition_variable cv;
	unique_lock<mutex>locker(mtx);
	cv.wait_for(locker, thread_sleep);
	cout << "           SOURCE 1 is  " << code << endl;
	locker.unlock();
	cv.notify_one();
}

void logger::PrintSrc2(string code) {
	mutex mtx;
	condition_variable cv;
	unique_lock<mutex>locker(mtx);
	cv.wait_for(locker, thread_sleep);
	cout << "           SOURCE 2 is  " << code << endl;
	locker.unlock();
	cv.notify_one();
}
void logger::PrintOpcode(string code) {
	mutex mtx;
	condition_variable cv;
	unique_lock<mutex>locker(mtx);
	cv.wait_for(locker, thread_sleep);
	cout << "           OPCODE is  " << code << endl;
	locker.unlock();
	cv.notify_one();
}