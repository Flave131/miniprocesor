#pragma once
#include <iostream>
#include <map>
#include <string>

using namespace std;

class Memory {

	map<uint16_t, uint16_t> map;

public:

	//Memory();

	Memory(string namefile="");

	uint64_t read(uint16_t addr);

	void write(uint16_t addr, uint16_t data);

	template<typename T> T readM(T const addr) {
		T nr = 0;
		int size = sizeof(nr) / sizeof(uint16_t);
		uint16_t temp = 0;

		for (int i = size - 1; i >= 0; i--) {
			if (map.count(addr + 2 * i) == 0) { //nothing found at addr
				temp = 0xffff;
			}
			else { //found smthing in memory
				temp = map[addr + 2 * i];
			}
			nr = nr << 16 | temp;
		}
		return nr;

	}

	void print();

	void printF(string namefile="");
};