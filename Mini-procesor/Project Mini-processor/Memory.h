#pragma once
#include <map>
#include <string>


#ifndef Memory.h

class Memory {
	
	map<uint16_t, uint16_t> map;

public:

	Memory(string namefile="");

	uint64_t read(uint16_t addr);

	void write(uint16_t addr, uint16_t data);

	void print();
};
#endif // !Memory.h