#include "Memory.h"
#include "CPU.h"
#include <iostream>
#include <vector>
#include <thread>
#include <queue>
#include <array>
#include <string>

 

using namespace std;

int main(void) {

	Memory M("input.txt");
	//M.print();

	//M.write(0x21, 0xff);

	//cout << endl <<hex<< M.read(0xfff0);

	//cout << endl << hex << M.readM<uint16_t>(0xfff0)<<endl;
	//cout << endl << hex << M.readM<uint32_t>(0xfff0) << endl;
	//cout << endl << hex << M.readM<uint64_t>(0xfff0) << endl;

	M.printF("output.txt");
	
	logger log;

	CPU cpu(&log, &M);

	cpu.start_sim();


	//cpu.stop_sim();


	system("pause");
	return 0;
}