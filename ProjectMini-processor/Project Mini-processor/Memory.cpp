#include "Memory.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <stdio.h>
#include <inttypes.h>
#include <cassert>

using namespace std;

Memory::Memory(string namefile) {

	string line;
	uint16_t address = 0;
	uint64_t data = 0;
	ifstream file;
	file.open(namefile);

	if (!file) {
		printf("Cannot open input file.\n");
	}
	try {
		int i = 0;
		while (!file.eof()) {
			getline(file, line);
			cout << dec << i << ". ";
			cout << hex << line << endl;
			i++;
			if (!(line.find_first_of('#',0))) {
				line.erase(0,1);
				address = stoi(line,nullptr,16);
				cout << "Found an address: " << hex<< address << endl;
				
			}
			else {
				data = stoi(line,nullptr,16);
				cout << "Found  data: " << hex<< data << endl;

				map[address] = data;
				address += 2;
			}
		}
		file.close();
	}
	catch (const std::exception & e) {
		cout << "Doesn't work : " << e.what() << endl;
		system("pause");
	}
}


uint64_t Memory::read(uint16_t addr) {

	uint64_t nr;
	//nr = map[addr] | map[addr+2] << 16 | map[addr+4] << 32 | map[addr+6] << 48;
	nr = 0;
	int aux;
	uint16_t temporar = 0;
	for (int i = 3; i >=0; i--) {
		if (map.count(addr+2*i)==0) { //Nothing at addr
			temporar = 0xffff;
		}
		else { //Something found
			temporar = map[addr+2*i];
		}
		nr = nr << 16 | temporar;
	}
	return nr;
}


void Memory::write(uint16_t addr, uint16_t data) {
	assert(addr % 2 == 0);
	map[addr] = data;
}

void Memory::print() {

	cout << "Printing the memmory:" << endl;
	for (auto& t : map) {
		cout << t.first << " " << t.second << "\n";
	}
}


void Memory::printF(string namefile) {
	
	ofstream output(namefile);
	cout << "Printing in "<< namefile << endl;
	for (auto& t : map) {
		output << hex << t.first << " " << t.second << "\n";
	}
	output.close();
}