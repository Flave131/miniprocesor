#include "CPU.h"
#include "Memory.h"
#include "Config.h"
#include <fstream>
#include <string>
#include <stdio.h>
#include <inttypes.h>
#include <thread>
#include <time.h>
#include <chrono>
#include <mutex>

using namespace std;

int globalcycle;
bool run = true;
bool fetch = false;
bool loading = false;

queue<uint64_t> IC_DE;
queue<uint64_t> IC_LSw;
queue<uint64_t> LS_ICw;
queue<uint64_t> IC_LS;
queue<uint64_t> LS_IC;
queue<uint64_t> DE_IC;
queue<uint64_t> EX_LS;
queue<uint64_t> LS_EX;
queue<uint64_t> EX_cBuff;
queue<uint16_t> window_ip;
queue<uint64_t> window_data;
queue<CPU::Instruct> hpp_DE_EX;
queue<CPU::Instruct> hpp_cBuff_EX;
queue<CPU::Instruct> vector_date;
mutex mtx;
condition_variable cv;
uint16_t header;
uint16_t saver = 0;
bool newIP = true;
bool necesar = false;
ofstream file;
uint16_t firstWindow;
uint16_t secondWindow;

void CPU::IC() {

	while (run) {

		if (globalcycle % delay_IC == 0) {

			int target_cycle = globalcycle + delay_IC;

			while (target_cycle > globalcycle) {
				std::this_thread::sleep_for(requested_sleep);
			}

			string send = "-- IC sends ip to LS -- ";
			string get = "-- IC recieved data from LS -- ";
			string mSend = "-- IC sends NEW ip to LS --";
			string mGet = "-- IC recieved MORE data from LS -- ";

			while (newIP == true) {
				firstWindow = IP & ~7;
				IC_LS.push(firstWindow);
				ptrLog->PrintIP(send, firstWindow);
				uint16_t vectorIP;
				newIP = false;
			}

			if (window_ip.size() != 0) {
				uint16_t newIp = window_ip.front();
				window_ip.pop();
				newIp = newIp & ~7;
				IC_LSw.push(newIp);
			
			}

			if (LS_ICw.size() != 0) {
				uint64_t newData = LS_ICw.front();
				IC_LSw.pop();
				window_data.push(newData);
			}

			if (LS_IC.size() != 0) {
				uint64_t data = LS_IC.front();
				LS_IC.pop();
				ptrLog->PrintData(get, data);
				IC_DE.push(data);
			}
		}
		else {
			std::this_thread::sleep_for(requested_sleep);
		}
	}
}

void CPU::LS() {

	while (run) {

		uint16_t ip;

		if (globalcycle % delay_LS == 0) {
			int target_cycle = globalcycle;
			while (target_cycle > globalcycle) {

			}
			string send = "-- LS sends data to IC --  ";
			string mSend = "-- LS sends data to EX --  ";
			string get = "-- LS recieved ip from IC -- ";
			string mGet = "-- LS recieved ip from EX -- ";

			if (IC_LS.size() != 0 && newIP == false) {
				ip = static_cast<uint16_t>(IC_LS.front());
				IC_LS.pop();
				ptrLog->PrintIP(get, ip);

				uint64_t data = memory->readM<uint64_t>(ip);
				LS_IC.push(data);
				ptrLog->PrintData(send, data);
			}

			if (IC_LSw.size() != 0) {
				ip = static_cast<uint16_t>(IC_LSw.front());
				//IC_LSw.pop();
				uint64_t newData = memory->readM<uint64_t>(ip);
				LS_ICw.push(newData);
			}

			if (EX_LS.size() != 0) { //requested more DATA by DE
				uint16_t loadIp = EX_LS.front();
				ptrLog->PrintIP(mGet, loadIp);

				uint64_t data = memory->readM<uint64_t>(loadIp);
				EX_LS.pop();
				LS_EX.push(data);
				ptrLog->PrintData(mSend, data);
			}
		}
		else {
			std::this_thread::sleep_for(requested_sleep);
		}
	}
}


void CPU::DE() {

	bool parameter1 = false;
	bool parameter2 = false;
	uint16_t opcode;
	uint16_t src1;
	uint16_t src2;
	Instruct Instr;
	uint16_t srcSaver;
	uint64_t data = 0;
	bool done = true;
	int size = 0;
	int currentPosition;
	uint16_t vector[4];
	uint16_t newVector[4];
	uint16_t ip1 = IP;

	while (run) {

		if (globalcycle % delay_DE == 0) {

			int target_cycle = globalcycle;
			while (target_cycle > globalcycle) {
				std::this_thread::sleep_for(thread_sleep);
			}

			string send = "-- DE sends data to EX -- ";
			string get = "-- DE received data from IC -- ";


			if (IC_DE.size() != 0) { //daca am data in IC_DE
				data = IC_DE.front();
				IC_DE.pop();
				ptrLog->PrintData(get, data);
				memcpy(vector, &data, sizeof(uint64_t));
				currentPosition = ((ip1 >> 1) % 4);
				done = false; // am luat data si urmeaza sa o parcurg

			LOOP: while (done == false && currentPosition < 4) {

				header = vector[currentPosition];

				src2 = header & mask_src2;
				src1 = header >> mask_src1;
				opcode = header >> mask_op;

				switch (opcode) {
				case add:
					ptrLog->PrintOpcode("ADD");
					break;
				case sub:
					ptrLog->PrintOpcode("SUB");
					break;
				case mov:
					ptrLog->PrintOpcode("MOV");
					break;
				case mul:
					ptrLog->PrintOpcode("MUL");
					break;
				case div:
					ptrLog->PrintOpcode("DIV");
					break;
				case cmp:
					ptrLog->PrintOpcode("CMP");
					break;
				case jmp:
					ptrLog->PrintOpcode("JMP");
					break;
				case je:
					ptrLog->PrintOpcode("JE");
					break;
				case jl:
					ptrLog->PrintOpcode("JL");
					break;
				case jg:
					ptrLog->PrintOpcode("JG");
					break;
				case jz:
					ptrLog->PrintOpcode("JZ");
					break;
				case call:
					ptrLog->PrintOpcode("CALL");
					break;
				case ret:
					ptrLog->PrintOpcode("RET");
					break;
				case end_sim:
					ptrLog->PrintOpcode("END SIM");
					break;
				case pushh:
					ptrLog->PrintOpcode("PUSH");
					break;
				case popp:
					ptrLog->PrintOpcode("POP");
					break;
				default:
					ptrLog->PrintOpcode("Not valid");
					header = 0x0;
					break;
				}

				if (src1 == ADDR || src1 == ADDR_R || src1 == IMM) { parameter1 = true; size++; }
				if (src2 == ADDR || src2 == ADDR_R || src2 == IMM) { parameter2 = true; size++; }

				if (currentPosition == 0 || currentPosition == 1) {
					if (parameter1) {
						Instr.optionalParameter1 = vector[++currentPosition];
						currentPosition++;
					}
					if (parameter2) { Instr.optionalParameter2 = vector[++currentPosition]; }

					Instr.header = header;
					vector_date.push(Instr);
					cout << "Complete instruction: " << endl << Instr.header << "  " << Instr.optionalParameter1 << "  " << Instr.optionalParameter2 << endl;
					currentPosition++;
					goto LOOP;
				}

				if (currentPosition == 2) {
					if (size == 0) {
						Instr.header = header;
						vector_date.push(Instr);
						currentPosition++;
						goto LOOP;
					}
					if (size == 1 && parameter1 == true) {
						Instr.optionalParameter1 = vector[++currentPosition];
						Instr.header = header;
						cout << "Complete instruction: " << endl << Instr.header << "  " << Instr.optionalParameter1 << "  " << Instr.optionalParameter2 << endl;
						vector_date.push(Instr);
						currentPosition++;
						done = true;
						goto LOOP;
					}
					if (size == 2) {
						//cout << "Incomplete instruction" << endl;
						Instr.header = header;
						Instr.optionalParameter1 = vector[++currentPosition];
						currentPosition++; //cP = 4 -> stop while

						ip1 = IP + 8;
						window_ip.push(ip1); // send to IC
						
						while (window_data.size() == 0) { //recieve data from IC
							std::this_thread::sleep_for(thread_sleep);
						}

						if (window_data.size() != 0) {
								uint64_t newWindow = window_data.front();
								window_data.pop();

								memcpy(newVector, &newWindow, sizeof(uint64_t));
								int index = 0;
								src2 = newVector[index];

								Instr.optionalParameter2 = src2;
								cout << "Complete instruction: " << endl << Instr.header << "  " << Instr.optionalParameter1 << "  " << Instr.optionalParameter2 << endl;

						}
						
						vector_date.push(Instr);
						done = true; // stop while
						goto LOOP;

					}
				}

				if (currentPosition == 3) {
					if (size == 0) {
						Instr.header = header;
						cout << "Instruction: " << endl << Instr.header << "  " << Instr.optionalParameter1 << "  " << Instr.optionalParameter2 << endl;
						vector_date.push(Instr);
						currentPosition++;
						done = true;
						goto LOOP;
					}

					if (size == 1 ) {
						ip1 = IP + 8;
						window_ip.push(ip1);
						uint64_t newWindow;

						while (window_data.size() == 0) { //recieve data from IC
							std::this_thread::sleep_for(thread_sleep);
						}

						if (window_data.size() != 0) {
							newWindow = window_data.front();
							window_data.pop();
							memcpy(newVector, &newWindow, sizeof(uint64_t));
							int index = 0;
							src1 = newVector[index];

							Instr.header = header;
							Instr.optionalParameter1 = src1;
							Instr.optionalParameter2 = 0;
							cout << "Complete instruction: " << endl << Instr.header << "  " << Instr.optionalParameter1 << endl;
						}

						vector_date.push(Instr);
						done = true; // stop while
						goto LOOP;
					}

					if (size == 2) {
						Instr.header = header;
						ip1 = IP + 2;
						window_ip.push(ip1);

						while (window_data.size() == 0) { //recieve data from IC
							std::this_thread::sleep_for(thread_sleep);
						}

						if (window_data.size() != 0) {
							uint64_t newWindow = window_data.front();
							window_data.pop();
							memcpy(newVector, &newWindow, sizeof(uint64_t));
							int index = 0;
							src1 = newVector[index];
							src2 = newVector[index + 1];

							Instr.optionalParameter1 = src1;
							Instr.optionalParameter2 = src2;
							cout << "Complete instruction: " << endl << Instr.header << "  " << Instr.optionalParameter1 << "  " << Instr.optionalParameter2 << endl;
						}

						vector_date.push(Instr);
						done = true; // stop while
						goto LOOP;
					}
				}
			} //while
				  if (currentPosition >= 3) {
					  //am parcurs data si trimit la EX
					  cout << "Number of instr.: " <<  vector_date.size() << endl;
					  for (int i = 0; i <= vector_date.size()+1; i++) {
						  Instr = vector_date.front();
						  cout << "******Instruction: " << endl << Instr.header << "  " << Instr.optionalParameter1 << "  " << Instr.optionalParameter2 << endl;
						  vector_date.pop();
						  hpp_DE_EX.push(Instr);
					  } // adauga toate instructiunile complete
				  }
				  else goto LOOP;
			}
		}
		else {
			//cout << " CLK " << globalcycle << "  is not right time for DE" << endl;
			std::this_thread::sleep_for(requested_sleep);
		}
	}
}

void CPU::EX() {

	while (run) {

		if (globalcycle % delay_EX == 0) {
			Instruct Instr;
			//Instruct copyInstr;
			uint16_t src1 = 0;
			uint16_t src2 = 0;
			//bool done = true;

			int target_cycle = globalcycle;
			while (target_cycle > globalcycle) {
				std::this_thread::sleep_for(thread_sleep);
			}

			string get = "-- EX recieved data from DE -- ";

			while (hpp_DE_EX.size() != 0) {

				Instr = hpp_DE_EX.front(); // scot o instrctiune
										   //copyInstr = Instr;
				hpp_DE_EX.pop();

				uint16_t instr = Instr.header;

				ptrLog->PrintData(get, instr);

				src2 = instr & 0x1F;

				src1 = (instr >> 0x5) & 0x1F;

				uint16_t opcode = (instr >> 0xA) & 0x3F;

				IP = IP + 2;

				switch (src1) {
				case R0:
					ptrLog->PrintSrc1("R0");
					break;
				case R1:
					ptrLog->PrintSrc1("R1");
					break;
				case R2:
					ptrLog->PrintSrc1("R2");
					break;
				case R3:
					ptrLog->PrintSrc1("R3");
					break;
				case R4:
					ptrLog->PrintSrc1("R4");
					break;
				case R5:
					ptrLog->PrintSrc1("R5");
					break;
				case R6:
					ptrLog->PrintSrc1("R6");
					break;
				case R7:
					ptrLog->PrintSrc1("R7");
					break;
				case IMM:
					IP = IP + 2;
					ptrLog->PrintSrc1("IMM");
					break;
				case ADDR:
					IP = IP + 2;
					ptrLog->PrintSrc1("ADDR");
					break;
				case ADDR_R:
					IP = IP + 2;
					ptrLog->PrintSrc1("ADDR_R");
					break;
				default:
					ptrLog->PrintSrc1("Not valid");
					break;
				}


				switch (src2) {
				case R0:
					ptrLog->PrintSrc2("R0");
					break;
				case R1:
					ptrLog->PrintSrc2("R1");
					break;
				case R2:
					ptrLog->PrintSrc2("R2");
					break;
				case R3:
					ptrLog->PrintSrc2("R3");
					break;
				case R4:
					ptrLog->PrintSrc2("R4");
					break;
				case R5:
					ptrLog->PrintSrc2("R5");
					break;
				case R6:
					ptrLog->PrintSrc2("R6");
					break;
				case R7:
					ptrLog->PrintSrc2("R7");
					break;
				case IMM:
					IP = IP + 2;
					ptrLog->PrintSrc2("IMM");
					break;
				case ADDR:
					IP = IP + 2;
					ptrLog->PrintSrc2("ADDR");
					break;
				case ADDR_R:
					IP = IP + 2;
					ptrLog->PrintSrc2("ADDR_R");
					break;
				default:
					ptrLog->PrintSrc2("Not valid");
					break;
				}

				if (Instr.optionalParameter2 != NULL) {
					src2 = Instr.optionalParameter2;
				}
				if (Instr.optionalParameter1 != NULL) {
					src1 = Instr.optionalParameter1;
				}

				switch (opcode) {
				case add:
					if (src1 != IMM) {
						src1 = src1 + src2;
						if (src1 == 0xffff || src1 == 0x0) flag[Z] = 1;
					}
					else {
						cout << endl << "           ADD: Illegal source1" << endl;
					}
					break;

				case sub:
					if (src1 != IMM) {
						src1 = src1 - src2;
						if (src1 == 0xffff || src1 == 0x0)  flag[Z] = 1;

					}
					else {
						cout << "           SUB: Illegal source1" << endl;
					}
					break;

				case mov:
					if (src1 != IMM) {
						src1 = src2;
					}
					else {
						cout << endl << "           MOV: Illegal source1" << endl;
					}
					break;

				case mul:
					src1 = src1 * src2;
					if (src1 == 0xffff || src1 == 0x0) {
						flag[Z] = 1;
					}
					break;

				case div:
					if (src2 != 0) {
						src1 = src1 / src2;
						R[0] = src1;
						R[1] = src1 % src2;
						ptrLog->PrintSrc1(to_string(src1));
						if (src1 == 0xffff || src1 == 0x0) {
							flag[Z] = 1;
						}
					}
					else {
						cout << endl << "         Illegal source2" << endl;
					}
					break;

				case cmp:
					if (src1 == src2) { flag[E] = 1; flag[Z] = 0; flag[G] = 0; }
					else if (src1 == 0 && src2 == 0) { flag[Z] = 1; flag[E] = 0; flag[G] = 0; }
					else if (src1 > src2) {flag[G] = 1;  flag[Z] = 0; flag[E] = 0; }
					else {flag[Z] = 0; flag[E] = 0; flag[G] = 0; } //flag is Lower
					break;

				case jmp:
					IP = src1;
					break;

				case je:
					if (flag[E]) IP = src1;
					break;

				case jl:
					if (!flag[E] && !flag[G]) IP = src1;
					break;

				case jg:
					if (flag[G]) IP = src1;
					break;

				case jz:
					if (flag[Z]) IP = src1;
					break;

				case call:
					IP = src1;
					stack_base.push(IP + 2);
					break;

				case ret:
					R[0] = stack_base.top();
					stack_base.pop();
					IP = R[0];
					break;

				case end_sim:
					stop_sim();
					break;

				case pushh:
					stack_base.push(src1);
					stack_pointer--;
					break;

				case popp:
					R[2] = stack_base.top();
					stack_base.pop();
					stack_pointer++;
					break;

				default:
					ptrLog->PrintOpcode("Error in EX");
				}
				
				newIP = true;
			}
			//else std::this_thread::sleep_for(requested_sleep);
		}
	}
}


void CPU::cBuffer() {
	//while (run) {			
	//}
}

uint32_t CPU::load(uint16_t addr) {
	uint32_t data = 0;
	cout << endl << "            Request more data" << endl;
	EX_LS.push(addr);
	while (data == 0) {
		if (LS_EX.size() != 0) {
			data = LS_EX.front();
			LS_EX.pop();
			return data;
		}
		else this_thread::sleep_for(thread_sleep);
	}
}

void CPU::store(uint16_t addr, uint16_t data) {
	memory->write(addr, data);
}


void CPU::Clock() {
	globalcycle = 0;
	while (run) {
		globalcycle++;
		std::this_thread::sleep_for(requested_sleep);
		//cout << "Next clock" << endl;
	}
}

void CPU::start_sim() {

	IP = 0x000;
	// creating threads
	auto icThread = std::thread(&CPU::IC, this);
	thread::id id_ic = icThread.get_id();
	cout << "Thread IC" << endl;

	auto deThread = std::thread(&CPU::DE, this);
	thread::id id_de = deThread.get_id();
	cout << "Thread DE" << endl;

	auto exThread = std::thread(&CPU::EX, this);
	thread::id id_ex = exThread.get_id();
	cout << "Thread EX" << endl;

	/*std::vector<std::thread> threads;
	for (int i = 0; i < max_ex; i++)
		threads.push_back(thread(&CPU::EX, this));


	cout << "Synchronizing all threads...\n";
	for (auto& th : threads) th.join();*/

	auto lsThread = std::thread(&CPU::LS, this);
	thread::id id_ls = lsThread.get_id();
	cout << "Thread LS" << endl;

	/*auto cBuffThread = std::thread(&CPU::cBuffer, this);
	thread::id id_cBuff = cBuffThread.get_id();
	cout << "Thread cBuff" << endl;
	*/
	auto clkThread = std::thread(&CPU::Clock, this);
	thread::id id_clk = clkThread.get_id();
	cout << "Thread CLK" << endl;

	clkThread.detach();
	//cout << "D" << endl;
	icThread.detach();
	//cout << "E" << endl;
	deThread.detach();
	//cout << "F" << endl;
	exThread.detach();
	//cout << "G" << endl;
	lsThread.detach();
	//cout << "H" << endl;
	//cBuffThread.detach();
}


/*void CPU::stop_sim(int stop) {
while (globalcycle < stop) {

std::this_thread::sleep_for(requested_sleep);
//cout << "Waiting for stop" << endl;
}
run = false;
cout << "Stopped" << endl;
}*/

void CPU::stop_sim() {
	run = false;
	cout << "Stopped! " << endl;
	file.close();
}

void logger::PrintIP(string caller, uint16_t message) {
	mutex mtx;
	condition_variable cv;
	unique_lock<mutex>locker(mtx);
	cv.wait_for(locker, thread_sleep);
	cout << dec << "\n Clk: " << globalcycle;
	cout << hex << "   Message: " << caller << "   IP: " << message << endl;


	//file.open("outputFile.txt", std::ios_base::app);
	//file << "CLK: " + to_string(globalcycle) + " Message: " + caller + "  " + to_string(message) << endl;

	locker.unlock();
	cv.notify_one();


}

void logger::PrintData(string caller, uint64_t message) {
	mutex mtx;
	condition_variable cv;
	unique_lock<mutex>locker(mtx);
	cv.wait_for(locker, thread_sleep);
	cout << dec << "\n Clk: " << globalcycle;
	cout << hex << "   Message: " << caller << "   Data: " << message << endl;

	//file.open("outputFile.txt", std::ios_base::app);
	//file << "CLK: " + to_string(globalcycle) + " Message: " + caller + "  " + to_string(message) << endl;

	locker.unlock();
	cv.notify_one();
}



void logger::PrintSrc1(string code) {
	mutex mtx;
	condition_variable cv;
	unique_lock<mutex>locker(mtx);
	cv.wait_for(locker, thread_sleep);
	cout << "           SOURCE 1 is  " << code << endl;
	locker.unlock();
	cv.notify_one();
}

void logger::PrintSrc2(string code) {
	mutex mtx;
	condition_variable cv;
	unique_lock<mutex>locker(mtx);
	cv.wait_for(locker, thread_sleep);
	cout << "           SOURCE 2 is  " << code << endl;
	locker.unlock();
	cv.notify_one();
}
void logger::PrintOpcode(string code) {
	mutex mtx;
	condition_variable cv;
	unique_lock<mutex>locker(mtx);
	cv.wait_for(locker, thread_sleep);
	cout << "           OPCODE is  " << code << endl;
	locker.unlock();
	cv.notify_one();
}
